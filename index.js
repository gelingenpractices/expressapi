const express = require('express');
const http = require('http');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const router = require('./router');
const mongoose = require('mongoose');

const app = express();

//DB Setup
mongoose.connect('mongodb://localhost:auth/auth');

//App Setup
//morgan - logging framework
app.use(morgan('combined'));
//bodyParser -> used to parse the incoming request
app.use(bodyParser.json({ type: '*/*' }));
router(app);

//Server Setup
const port = process.env.PORT || 3090;
const server = http.createServer(app);
server.listen(port);
console.log('Server running on port:', port);
