const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Define the model
const userSchema = new Schema({
    email: {
       type: String,
       unique: true,
       lowercase: true
    },
    password: String
});

//Create the model
const ModelClass = mongoose.model('user', userSchema);

//Export for usage
module.exports = ModelClass;