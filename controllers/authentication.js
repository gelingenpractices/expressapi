const User = require('../models/user');
const jwt = require('jwt-simple');
const config = require('../config');

function tokenForUser(user){
    const timestamp = new Date().getTime();
    return jwt.encode({ sub: user.id, iat: timestamp }, config.secret);
}

exports.signin = function(req, res, next){
    return res.json({ token: tokenForUser(req.user) });
}

exports.signup = function(req, res, next){
    const email = req.body.email;
    const password = req.body.password;

    if(!email  || !password)
        return res.status(422).send({error: 'Provide user/password for processing...'});

    User.findOne({ email: email }, function(err, existingUser){
        if(err){
            return next(err);
        }

        //if user already exists...return bad request....
        if(existingUser){
            return res.status(422).send({error: 'Email in use by another user'});
        }

        //create new user
        const user = new User({
            email: email,
            password: password
        });

        user.save(function(err, user){
            if(err)
                return next(err);
            
            return res.json({ token: tokenForUser(user) });
        });
    });

}