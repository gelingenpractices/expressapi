const auth = require('./controllers/authentication');
const passwordService = require('./services/passport');
const passport = require('passport');

const requireAuth = passport.authenticate('jwt', { session: false });
const requireSignIn = passport.authenticate('local', { session: false });

module.exports = function(app){
    app.get('/', requireAuth, function(req, res){
        res.send("Hi there");
    });

    app.post('/signin', requireSignIn, auth.signin);
    app.post('/signup', auth.signup);
}